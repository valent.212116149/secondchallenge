import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ImagealbumService } from '../imagealbum.service';
import {Observable} from 'rxjs';
import {IonSlides} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  albums: {id: string, title: string}[] = [];
  @ViewChild('mySlider') slider: IonSlides;

  constructor(private http: HttpClient,
              private iaService: ImagealbumService) {
  }

  ngOnInit(): void {
    this.albums = this.iaService.showAlbums();
  }
}
