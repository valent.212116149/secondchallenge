import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImagealbumService {
  albums;
  images;

  constructor(private http: HttpClient) { }
  showAlbums(){
    this.albums = [];

    this.http.get('https://jsonplaceholder.typicode.com/albums').subscribe((responseAlbum: any) => {
      (responseAlbum).forEach(obj => {
        this.albums.push({id: obj.id, title: obj.title});
      });
    });
    return this.albums;
  }
  showImages(id: any){
    this.images = [];

    this.http.get('https://jsonplaceholder.typicode.com/albums/' + id + '/photos').subscribe((responseAlbum: any) => {
      (responseAlbum).forEach(obj => {
        this.images.push({id: obj.id, title: obj.url});
      });
    });
    return this.images;
  }
}
