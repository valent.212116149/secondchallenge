import {Component, OnInit, ViewChild} from '@angular/core';
import {ImagealbumService} from '../imagealbum.service';
import {ActivatedRoute} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {ModaldetailPage} from './modaldetail/modaldetail.page';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  images: {id: string, title: string}[] = [];
  myId;
  dataReturned: any;
  buttonText: any;
  buttonIcon: any;

  constructor(private iaService: ImagealbumService,
              private activatedRoute: ActivatedRoute,
              public modalController: ModalController,
              ) {
  }

  ngOnInit() {
    this.myId = this.activatedRoute.snapshot.paramMap.get('id');
    this.images = this.iaService.showImages(this.myId);
  }

  async openModal(paramImages, paramId) {
    const modal = await this.modalController.create({
      component: ModaldetailPage,
      componentProps: {
        images: paramImages,
        id: paramId,
        album: this.myId
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        // alert('Modal Sent Data :'+ dataReturned);
      }
    });

    return await modal.present();
  }

  /*loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.albums.length === 100) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }*/

}
