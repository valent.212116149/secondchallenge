import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {IonSlides, LoadingController, ModalController, NavParams} from '@ionic/angular';

@Component({
  selector: 'app-modaldetail',
  templateUrl: './modaldetail.page.html',
  styleUrls: ['./modaldetail.page.scss'],
})
export class ModaldetailPage implements OnInit {
  modalImages: any;
  modalId: number;
  modalAlbum: number;
  @ViewChild('slide', { static: false }) slide: IonSlides;

  constructor(
      private modalController: ModalController,
      private navParams: NavParams,
      public loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.modalImages = this.navParams.data.images;
    this.modalId = this.navParams.data.id;
    this.modalAlbum = this.navParams.data.album;
    /*console.log(this.modalId);
    console.log(this.modalAlbum);
    console.log( this.modalId - ( (this.modalAlbum - 1) * 50 ) );*/
    this.goToSlide(this.modalId - ( (this.modalAlbum - 1) * 50 ) - 1);
  }

  slideChanged() {
    const currentIndex = this.slide.getActiveIndex();
  }

  goToSlide(id) {
    this.presentLoading();
    setTimeout(() => {
      this.slide.slideTo(id);
      // console.log(id);
    }, 1000);
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }

}
