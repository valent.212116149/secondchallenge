import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModaldetailPageRoutingModule } from './modaldetail-routing.module';

import { ModaldetailPage } from './modaldetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModaldetailPageRoutingModule
  ],
  declarations: [ModaldetailPage]
})
export class ModaldetailPageModule {}
