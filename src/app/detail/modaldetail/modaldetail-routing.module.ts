import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModaldetailPage } from './modaldetail.page';

const routes: Routes = [
  {
    path: '',
    component: ModaldetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModaldetailPageRoutingModule {}
